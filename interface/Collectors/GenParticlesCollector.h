#ifndef EMTFTools_NtupleMaker_GenParticlesCollector_h
#define EMTFTools_NtupleMaker_GenParticlesCollector_h

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"
// #include "MagneticField/Engine/interface/MagneticField.h"
// #include "MuonAnalysis/MuonAssociators/interface/PropagateToMuonSetup.h"

namespace emtf::tools {

    class GenParticlesCollector: public DataCollector {
        public:
            GenParticlesCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&,
                    edm::ConsumesCollector&&
            );

            ~GenParticlesCollector();

            void registerFields(DynamicTree&) const final;

            void collect(
                    const edm::Event&, const edm::EventSetup&, 
                    DynamicTree&
            ) const final;

        private:
            const NtupleMakerContext& context_;

            // PropagateToMuonSetup mu_propagator_st1_setup_;
            // PropagateToMuonSetup mu_propagator_st2_setup_;

            edm::EDGetTokenT<reco::GenParticleCollection> gen_part_token_;
            // edm::ESGetToken<MagneticField, IdealMagneticFieldRecord> bfield_token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_GenParticlesCollector_h
