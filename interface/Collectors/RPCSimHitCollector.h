#ifndef EMTFTools_NtupleMaker_RPCSimHitCollector_h
#define EMTFTools_NtupleMaker_RPCSimHitCollector_h

#include <cassert>
#include <cstdint>
#include <map>
#include <memory>
#include <utility>
#include <vector>

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "Geometry/RPCGeometry/interface/RPCGeometry.h"
#include "Geometry/Records/interface/MuonGeometryRecord.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"
#include "SimDataFormats/TrackingHit/interface/PSimHit.h"
#include "SimDataFormats/TrackingHit/interface/PSimHitContainer.h"
#include "SimDataFormats/CrossingFrame/interface/CrossingFrame.h"

namespace emtf::tools {

    class RPCSimHitCollector : public DataCollector {
        public:
            RPCSimHitCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&, 
                    edm::ConsumesCollector&&
            );
    
            ~RPCSimHitCollector();
    
            void registerFields(DynamicTree&) const final;
    
            void collect(
                    const edm::Event&, 
                    const edm::EventSetup&, 
                    DynamicTree&
            ) const final;
    
        private:
            const NtupleMakerContext& context_;

            const bool crossing_frame_en_;
    
            edm::EDGetTokenT<edm::PSimHitContainer>          sim_hits_token_;
            edm::EDGetTokenT<CrossingFrame<PSimHit>>         sim_hits_xf_token_;
            edm::ESGetToken<RPCGeometry, MuonGeometryRecord> geom_token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_RPCSimHitCollector_h not defined
