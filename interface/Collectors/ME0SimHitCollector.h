#ifndef EMTFTools_NtupleMaker_ME0SimHitCollector_h
#define EMTFTools_NtupleMaker_ME0SimHitCollector_h

#include <cassert>
#include <cstdint>
#include <map>
#include <memory>
#include <utility>
#include <vector>

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "Geometry/GEMGeometry/interface/ME0Geometry.h"
#include "Geometry/Records/interface/MuonGeometryRecord.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"
#include "SimDataFormats/TrackingHit/interface/PSimHit.h"
#include "SimDataFormats/TrackingHit/interface/PSimHitContainer.h"
#include "SimDataFormats/CrossingFrame/interface/CrossingFrame.h"

namespace emtf::tools {

    class ME0SimHitCollector : public DataCollector {
        public:
            ME0SimHitCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&, 
                    edm::ConsumesCollector&&
            );

            ~ME0SimHitCollector();

            void registerFields(DynamicTree&) const final;

            void collect(
                    const edm::Event&, 
                    const edm::EventSetup&, 
                    DynamicTree&
            ) const final;

        private:
            const NtupleMakerContext& context_;

            const bool crossing_frame_en_;

            edm::EDGetTokenT<edm::PSimHitContainer>          sim_hits_token_;
            edm::EDGetTokenT<CrossingFrame<PSimHit>>         sim_hits_xf_token_;
            edm::ESGetToken<ME0Geometry, MuonGeometryRecord> geom_token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_ME0SimHitCollector_h not defined
