#ifndef EMTFTools_NtupleMaker_DynamicTree_h
#define EMTFTools_NtupleMaker_DynamicTree_h

#include <any>
#include <map>
#include <string>
#include <vector>

#include "TTree.h"

namespace emtf::tools {

    namespace fields {
        // If a variable is a container, clear the container. Otherwise, reset the variable to zero.
        template <typename T, typename = void>
            struct is_clearable : std::false_type {};

        template <typename T>
            struct is_clearable<T, std::void_t<decltype(std::declval<T>().clear())>> : std::true_type {};

        template <typename T>
            inline constexpr bool is_clearable_v = is_clearable<T>::value;

        template <typename T>
            typename std::enable_if_t<is_clearable_v<T>> reset_value(T& value) {
                value.clear();
            }

        template <typename T>
            typename std::enable_if_t<!is_clearable_v<T>> reset_value(T& value) {
                value = T{};
            }

        class FieldInterface {
            public:
                FieldInterface() = default;
                
                virtual ~FieldInterface() = default;

                virtual void clear() = 0;
        };

        template<typename T>
            class Field: public FieldInterface {
                public:
                    Field() = default; 

                    ~Field() = default;

                    T value_;

                    void clear() override {
                        reset_value(value_);
                    }
            };
    }

    class DynamicTree {
        public:
            DynamicTree();

            ~DynamicTree();

            void fill();

            void clear();

            void write();

        private:
            TTree* tree_;
            std::map<std::string, std::unique_ptr<fields::FieldInterface>> fields_;

        public:
            // Base Methods
            template<typename T>
                void decfield(const std::string& name) {
                    // Short-Circuit: Already registered
                    const auto lookup_name = fields_.find(name);

                    if (lookup_name != fields_.end()) {
                        return;
                    }

                    // Allocate space
                    fields_[name] = std::make_unique<fields::Field<T>>();

                    // Register branch
                    tree_->Branch(
                            name.c_str(), 
                            &(dynamic_cast<fields::Field<T>*>(fields_[name].get())->value_), 
                            256000
                    );
                }

            template<typename T>
                T& get(const std::string& name) {
                    return dynamic_cast<fields::Field<T>*>(fields_[name].get())->value_;
                }

            template<typename T>
                void set(const std::string& name, const T& new_value) {
                    dynamic_cast<fields::Field<T>*>(fields_[name].get())->value_ = new_value;
                }

            // Tag Methods
            template<typename T>
                void decfield() {
                    decfield<typename T::type>(T::name);
                }

            template<typename T>
                typename T::type& get() {
                    return get<typename T::type>(T::name);
                }

            template<typename T>
                void set(const typename T::type& new_value) {
                    set(T::name, new_value);
                }
    };
}

#endif // namespace EMTFTools_NtupleMaker_DynamicTree_h

