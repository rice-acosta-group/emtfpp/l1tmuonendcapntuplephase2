#include <cmath>

#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"

namespace emtf::tools {

    double calcInvpt(const double& charge, const double& pt) {
        double invpt = (std::abs(pt) < 1e-15) ? 1e-15 : std::abs(pt); // ensures pt > 0, protects against division by zero
        invpt = 1.0 / pt;
        invpt = (invpt < 1e-15) ? 1e-15 : invpt;                      // protects against zero
        invpt *= charge;                                              // charge should be +/-1
        return invpt;
    }

    double calcDxy(const double& invpt, const double& phi, const double& vx, const double& vy) {
        constexpr double B = 3.811;                            // in Tesla
        double rg = -1.0 / (0.003 * B * invpt);                // R = -pt / (0.003 q B)  [cm]

        double xc = vx - (rg * std::sin(phi));                  // cx = vx - R sin(phi)
        double yc = vy + (rg * std::cos(phi));                  // cy = vy + R cos(phi)
        double cg = std::hypot(xc, yc);

        return rg - (cg * rg / std::abs(rg));
    }

    double calcD0(const double& invpt, const double& phi, const double& vx, const double& vy) {
        constexpr double B = 3.811;                            // in Tesla
        double rg = -1.0 / (0.003 * B * invpt);                // R = -pt / (0.003 q B)  [cm]

        double xc = vx - (rg * std::sin(phi));
        double yc = vy + (rg * std::cos(phi));

        double cg = std::hypot(xc, yc);
        double xc_norm = xc / cg;
        double yc_norm = yc / cg;

        double dxy = cg - (rg * rg / std::abs(rg));
        double xd0 = dxy * xc_norm;
        double yd0 = dxy * yc_norm;
        double udxy = std::hypot(xd0, yd0);

        double lxy = std::hypot(vx, vy);
        double lxy_dot_d0 = vx * xd0 + vy * yd0;
        double lxy_d0_ang = std::acos(lxy_dot_d0 / lxy / udxy);

        if (lxy_d0_ang == 0) {
            return 0;
        } else if (lxy_d0_ang < (M_PI / 2)) {
            return udxy;
        } else {
            return -udxy;
        }
    }

}
