#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

#include "EMTFTools/NtupleMaker/interface/DynamicTree.h"

using namespace emtf::tools;

DynamicTree::DynamicTree() {
    edm::Service<TFileService> file_service;

    tree_ = file_service->make<TTree>("tree", "tree");

    if (tree_ == nullptr) {
        throw edm::Exception(edm::errors::FatalRootError) << "Failed to create the tree.";
    }
}

DynamicTree::~DynamicTree() {
    // NOTE: The tree will be deleted by the file service so don't delete it!
    // Do nothing
}

void DynamicTree::fill() {
    tree_->Fill();
}

void DynamicTree::write() {
    // NOTE: The tree will be writte by the file service so don't write it!
    // Do nothing
}

void DynamicTree::clear() {
    for (auto& kf : fields_) {
        auto& field_ptr = kf.second;
        field_ptr->clear(); 
    } 
}

