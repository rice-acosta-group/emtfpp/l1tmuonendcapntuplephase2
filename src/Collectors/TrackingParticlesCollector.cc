#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/TrackingParticlesCollector.h"

using namespace emtf::tools;

TrackingParticlesCollector::TrackingParticlesCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    token_  = consumes_collector.consumes<TrackingParticleCollection>(
            pset.getParameter<edm::InputTag>("TrkPartTag")  
    );
}

TrackingParticlesCollector::~TrackingParticlesCollector() {
    // Do Nothing
}

void TrackingParticlesCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_trk_part_id>();
    tree.decfield<tag_trk_part_pt>();
    tree.decfield<tag_trk_part_phi>();
    tree.decfield<tag_trk_part_theta>();
    tree.decfield<tag_trk_part_eta>();
    tree.decfield<tag_trk_part_vx>();
    tree.decfield<tag_trk_part_vy>();
    tree.decfield<tag_trk_part_vz>();
    tree.decfield<tag_trk_part_invpt>();
    tree.decfield<tag_trk_part_d0>();
    tree.decfield<tag_trk_part_dxy>();
    tree.decfield<tag_trk_part_beta>();
    tree.decfield<tag_trk_part_mass>();
    tree.decfield<tag_trk_part_q>();
    tree.decfield<tag_trk_part_bx>();
    tree.decfield<tag_trk_part_event>();
    tree.decfield<tag_trk_part_pdgid>();
    tree.decfield<tag_trk_part_status>();
    tree.decfield<tag_trk_part_decay>();
    tree.decfield<tag_trk_part_genp>();
    tree.decfield<tag_trk_part_important>();
    tree.decfield<tag_vsize_trk_part>();
}

void TrackingParticlesCollector::collect(
        const edm::Event& event,
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get Tracking Particles
    auto handle = event.getHandle(token_);

    const auto trk_part_collection = *reinterpret_cast<const TrackingParticleCollection*>(
            handle.product()
    );

    // Collect Traking Particles
    int tp_inc_count = 0;
    int tp_keep_count = 0;

    for (const auto& trk_particle : trk_part_collection) {
        // Increase Inclusive Tracking Particle Count
        tp_inc_count += 1;

        // Short-Circuit: Keep leptons, W/Z/H, charged particles, and high-pt gamma.
        // For meaning of status see: https://github.com/cms-sw/cmssw/blob/master/DataFormats/HepMCCandidate/interface/GenStatusFlags.h
        int pdgid = std::abs(trk_particle.pdgId());

        bool keep_particle = (pdgid == 13);
        keep_particle |= ((pdgid == 23) or (pdgid == 24) or (pdgid == 25) or (pdgid == 37));
        keep_particle |= (((pdgid == 11) or (pdgid == 15)) and (trk_particle.pt() > 1.));
        keep_particle |= (((pdgid == 211) or (pdgid == 321) or (pdgid == 2212)) and (trk_particle.pt() > 1.));
        keep_particle |= ((trk_particle.charge() != 0) and (trk_particle.status() == 1) and (trk_particle.pt() > 2.));  // charged above 2 GeV
        keep_particle |= ((pdgid == 22) and (trk_particle.status() == 1) and (trk_particle.pt() > 10.));                // gamma above 10 GeV
        keep_particle |= (((1000001 <= pdgid) and (pdgid <= 1000039)) or ((2000001 <= pdgid) and (pdgid <= 2000015)));  // SUSY fiction particles
    
        if (!keep_particle) {
            continue;
        }

        // Short-Circuit: In time + out of time bunch-crossing
        bool kindof_intime = ((-2 <= trk_particle.eventId().bunchCrossing()) and (trk_particle.eventId().bunchCrossing() <= 2));

        if (!kindof_intime) {
            continue;
        }
  
        // Primary: pt > 0.1 GeV, |eta| < 3.0, |rho0| < 0.5 cm, |z0| < 30 cm
        // bool primary = ((part.pt() > 0.1) and (std::abs(part.eta()) <= 3.0) and
        //                (std::hypot(part.vx(), part.vy()) < 0.5) and (std::abs(part.vz()) < 30.0));
        // Primary + secondary: pt > 0.1 GeV, |eta| < 5.0, |x0| < 600 cm, |y0| < 600 cm, |z0| < 1200 cm
        bool kindof_primary = ((trk_particle.pt() > 0.1) and (std::abs(trk_particle.eta()) <= 5.0) 
                and (std::abs(trk_particle.vx()) <= 600.) and (std::abs(trk_particle.vy()) <= 600.) and (std::abs(trk_particle.vz()) <= 1200.));

        // trk particles coming from gen particles
        bool from_genp = (not trk_particle.genParticles().empty());

        if (!(kindof_primary || from_genp)) {
            continue;
        }

        // Short-Circuit: Remove trk particles without muon hits
        bool killed = ((trk_particle.status() == -99) and (trk_particle.genParticles().empty()) and
                       ((trk_particle.numberOfHits() == 0) or (trk_particle.numberOfHits() == trk_particle.numberOfTrackerHits())));

        if (killed) {
            continue;
        }

        // Increase Tracking Particle Kept Counter
        tp_keep_count += 1;

        // Find Gen Particle
        int genp = -1;

        if (!trk_particle.genParticles().empty()) {
            auto ref_key = (trk_particle.genParticles().begin())->key();
            genp = ref_key;
        }

        // Lookup gen particle
        bool is_important = false;

        if (genp > -1) {
            is_important = context_.gen_part_descendent_finder_.isImportant(genp);
        }

        // Fill event
        tree.get<tag_trk_part_id>().push_back(tp_inc_count - 1); // Store tracking particle id before cuts were applied
        tree.get<tag_trk_part_pt>().push_back(trk_particle.pt());
        tree.get<tag_trk_part_phi>().push_back(trk_particle.phi());
        tree.get<tag_trk_part_theta>().push_back(trk_particle.theta());
        tree.get<tag_trk_part_eta>().push_back(trk_particle.eta());
        tree.get<tag_trk_part_vx>().push_back(trk_particle.vx());
        tree.get<tag_trk_part_vy>().push_back(trk_particle.vy());
        tree.get<tag_trk_part_vz>().push_back(trk_particle.vz());
        tree.get<tag_trk_part_invpt>().push_back(calcInvpt(trk_particle.charge(), trk_particle.pt()));
        tree.get<tag_trk_part_d0>().push_back(calcD0(calcInvpt(trk_particle.charge(), trk_particle.pt()), trk_particle.phi(), trk_particle.vx(), trk_particle.vy()));
        tree.get<tag_trk_part_dxy>().push_back(calcDxy(calcInvpt(trk_particle.charge(), trk_particle.pt()), trk_particle.phi(), trk_particle.vx(), trk_particle.vy()));
        tree.get<tag_trk_part_beta>().push_back(trk_particle.p() / trk_particle.energy());
        tree.get<tag_trk_part_mass>().push_back(trk_particle.mass());
        tree.get<tag_trk_part_q>().push_back(trk_particle.charge());
        tree.get<tag_trk_part_bx>().push_back(trk_particle.eventId().bunchCrossing());
        tree.get<tag_trk_part_event>().push_back(trk_particle.eventId().event());
        tree.get<tag_trk_part_pdgid>().push_back(trk_particle.pdgId());
        tree.get<tag_trk_part_status>().push_back(trk_particle.status());
        tree.get<tag_trk_part_decay>().push_back(trk_particle.decayVertices().size());
        tree.get<tag_trk_part_genp>().push_back(genp);
        tree.get<tag_trk_part_important>().push_back(is_important);
    }

    tree.set<tag_vsize_trk_part>(tp_keep_count);
}

