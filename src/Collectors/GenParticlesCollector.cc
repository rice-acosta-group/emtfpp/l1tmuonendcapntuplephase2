#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"
// // vertex
// #include "DataFormats/VertexReco/interface/Vertex.h"
// #include "DataFormats/VertexReco/interface/VertexFwd.h"
// // track extrapolation
// #include "CondFormats/AlignmentRecord/interface/TrackerSurfaceDeformationRcd.h"
// #include "DataFormats/GeometryVector/interface/GlobalVector.h"
// #include "DataFormats/GeometryVector/interface/GlobalPoint.h"
// #include "TrackingTools/Records/interface/TrackingComponentsRecord.h"
// #include "TrackingTools/TrajectoryState/interface/TrajectoryStateOnSurface.h"
// #include "TrackingTools/TrajectoryState/interface/FreeTrajectoryState.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/GenParticlesCollector.h"

using namespace emtf::tools;

GenParticlesCollector::GenParticlesCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    gen_part_token_  = consumes_collector.consumes<reco::GenParticleCollection>(
            pset.getParameter<edm::InputTag>("GenPartTag")  
    );
}

GenParticlesCollector::~GenParticlesCollector() {
    // Do Nothing
}

void GenParticlesCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_gen_part_id>();
    tree.decfield<tag_gen_part_pt>();
    tree.decfield<tag_gen_part_phi>();
    tree.decfield<tag_gen_part_theta>();
    tree.decfield<tag_gen_part_eta>();
    tree.decfield<tag_gen_part_vx>();
    tree.decfield<tag_gen_part_vy>();
    tree.decfield<tag_gen_part_vz>();
    tree.decfield<tag_gen_part_invpt>();
    tree.decfield<tag_gen_part_d0>();
    tree.decfield<tag_gen_part_dxy>();
    tree.decfield<tag_gen_part_beta>();
    tree.decfield<tag_gen_part_mass>();
    tree.decfield<tag_gen_part_q>();
    tree.decfield<tag_gen_part_pdgid>();
    tree.decfield<tag_gen_part_parent_id>();
    tree.decfield<tag_gen_part_status>();
    tree.decfield<tag_gen_part_decay>();
    tree.decfield<tag_gen_part_genp>();
    tree.decfield<tag_gen_part_important>();
    tree.decfield<tag_vsize_gen_part>();
}

void GenParticlesCollector::collect(
        const edm::Event& event,
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get Gen Particles
    auto gen_part_handle = event.getHandle(gen_part_token_);

    const auto gen_part_collection = *reinterpret_cast<const reco::GenParticleCollection*>(
            gen_part_handle.product()
    );

    // Collect Gen Particles
    int gp_count = 0;
    int gp_keep_count = 0;

    for (const auto& gen_particle : gen_part_collection) {
        // Next Id
        gp_count += 1;

        // Short-Circuit: Keep leptons, W/Z/H, charged particles, and high-pt gamma.
        // For meaning of status see: https://github.com/cms-sw/cmssw/blob/master/DataFormats/HepMCCandidate/interface/GenStatusFlags.h
        int pdgid = std::abs(gen_particle.pdgId());

        bool keep_particle = (pdgid == 13);
        keep_particle |= ((pdgid == 23) or (pdgid == 24) or (pdgid == 25) or (pdgid == 37));
        keep_particle |= (((pdgid == 11) or (pdgid == 15)) and (gen_particle.pt() > 1.));
        keep_particle |= (((pdgid == 211) or (pdgid == 321) or (pdgid == 2212)) and (gen_particle.pt() > 1.));
        keep_particle |= ((gen_particle.charge() != 0) and (gen_particle.status() == 1) and (gen_particle.pt() > 2.));  // charged above 2 GeV
        keep_particle |= ((pdgid == 22) and (gen_particle.status() == 1) and (gen_particle.pt() > 10.));                // gamma above 10 GeV
        keep_particle |= (((1000001 <= pdgid) and (pdgid <= 1000039)) or ((2000001 <= pdgid) and (pdgid <= 2000015)));  // SUSY fiction particles
    
        if (!keep_particle) {
            continue;
        }
  
        // Primary: pt > 0.1 GeV, |eta| < 3.0, |rho0| < 0.5 cm, |z0| < 30 cm
        // bool primary = ((part.pt() > 0.1) and (std::abs(part.eta()) <= 3.0) and
        //                (std::hypot(part.vx(), part.vy()) < 0.5) and (std::abs(part.vz()) < 30.0));
        // Primary + secondary: pt > 0.1 GeV, |eta| < 5.0, |x0| < 600 cm, |y0| < 600 cm, |z0| < 1200 cm
        bool kindof_primary = ((gen_particle.pt() > 0.1) and (std::abs(gen_particle.eta()) <= 5.0) 
                and (std::abs(gen_particle.vx()) <= 600.) and (std::abs(gen_particle.vy()) <= 600.) and (std::abs(gen_particle.vz()) <= 1200.));

        if (!kindof_primary) {
            continue;
        }

        // Find parent id
        int parent_id = -9999;

        if (gen_particle.numberOfMothers() > 0) {
            parent_id = dynamic_cast<const reco::GenParticle *>(gen_particle.mother(0))->pdgId();
        }

        if (parent_id == gen_particle.pdgId()) {
            continue;
        }

        // Increase Gen Particle Kept Counter
        gp_keep_count += 1;

        // Lookup gen particle
        auto gp_id = gp_count - 1;

        auto is_important = context_.gen_part_descendent_finder_.isImportant(gp_id);

        // Fill event
        tree.get<tag_gen_part_id>().push_back(gp_id);
        tree.get<tag_gen_part_pt>().push_back(gen_particle.pt());
        tree.get<tag_gen_part_phi>().push_back(gen_particle.phi());
        tree.get<tag_gen_part_theta>().push_back(gen_particle.theta());
        tree.get<tag_gen_part_eta>().push_back(gen_particle.eta());
        tree.get<tag_gen_part_vx>().push_back(gen_particle.vx());
        tree.get<tag_gen_part_vy>().push_back(gen_particle.vy());
        tree.get<tag_gen_part_vz>().push_back(gen_particle.vz());
        tree.get<tag_gen_part_invpt>().push_back(calcInvpt(gen_particle.charge(), gen_particle.pt()));
        tree.get<tag_gen_part_d0>().push_back(calcD0(calcInvpt(gen_particle.charge(), gen_particle.pt()), gen_particle.phi(), gen_particle.vx(), gen_particle.vy()));
        tree.get<tag_gen_part_dxy>().push_back(calcDxy(calcInvpt(gen_particle.charge(), gen_particle.pt()), gen_particle.phi(), gen_particle.vx(), gen_particle.vy()));
        tree.get<tag_gen_part_beta>().push_back(gen_particle.p() / gen_particle.energy());
        tree.get<tag_gen_part_mass>().push_back(gen_particle.mass());
        tree.get<tag_gen_part_q>().push_back(gen_particle.charge());
        tree.get<tag_gen_part_pdgid>().push_back(gen_particle.pdgId());
        tree.get<tag_gen_part_parent_id>().push_back(parent_id);
        tree.get<tag_gen_part_status>().push_back(gen_particle.status());
        tree.get<tag_gen_part_important>().push_back(is_important);
    }

    tree.set<tag_vsize_gen_part>(gp_keep_count);
}

