#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2SimInfoCollector.h"

using namespace emtf::tools;

EMTFP2SimInfoCollector::EMTFP2SimInfoCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    hit_token_  = consumes_collector.consumes<emtf::phase2::EMTFHitCollection>(
            pset.getParameter<edm::InputTag>("EMTFP2HitTag")
    );

    part_token_  = consumes_collector.consumes<TrackingParticleCollection>(
            pset.getParameter<edm::InputTag>("TrkPartTag")  
    );
}

EMTFP2SimInfoCollector::~EMTFP2SimInfoCollector() {
    // Do Nothing
}

void EMTFP2SimInfoCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_max_hit_sim_pt>();          
}

void EMTFP2SimInfoCollector::collect(
        const edm::Event& event, 
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get hit collection
    auto hit_handle = event.getHandle(hit_token_);

    const auto hit_collection = *reinterpret_cast<const emtf::phase2::EMTFHitCollection*>(
            hit_handle.product()
    );

    // Get Tracking Particles
    auto part_handle = event.getHandle(part_token_);

    const auto trk_part_collection = *reinterpret_cast<const TrackingParticleCollection*>(
            part_handle.product()
    );

    // Collect hits
    float max_pt = -1;
    
    for (const auto& hit : hit_collection) {
        // Lookup tp
        const auto [sim_tp1, sim_tp2] = context_.subsystem_mc_truth_.findTrackingParticlesFromEMTFHit(hit);

        // Check if tp1 has max pt
        if (sim_tp1 > -1) {
            const auto& trk_particle = trk_part_collection[sim_tp1];

            // Max pT
            if (max_pt < trk_particle.pt()) {
                max_pt = trk_particle.pt();
            }
        }

        // Check if tp2 has max pt
        if (sim_tp2 > -1) {
            const auto& trk_particle = trk_part_collection[sim_tp2];

            // Max pT
            if (max_pt < trk_particle.pt()) {
                max_pt = trk_particle.pt();
            }
        }
    }

    tree.set<tag_max_hit_sim_pt>(max_pt);
}

