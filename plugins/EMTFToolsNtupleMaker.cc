#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/CSCSimHitCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2HitsCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2InputsCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2TracksCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2SimInfoCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/EventInfoCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/GEMSimHitCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/L1TrackTriggerCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/ME0SimHitCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/RPCSimHitCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/TrackingParticlesCollector.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/GenParticlesCollector.h"

#include "EMTFToolsNtupleMaker.h"

using namespace emtf::tools;

EMTFToolsNtupleMaker::EMTFToolsNtupleMaker(const edm::ParameterSet& pset):
    context_(pset, consumesCollector())
{
    // SharedResources
    usesResource("TFileService");

    // Register Collectors
    if (pset.getParameter<bool>("EventInfoEnabled")) {
        data_collectors_.push_back(std::make_unique<EventInfoCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("EMTFP2InputsEnabled")) {
        data_collectors_.push_back(std::make_unique<EMTFP2InputsCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("EMTFP2HitsEnabled")) {
        data_collectors_.push_back(std::make_unique<EMTFP2HitsCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("EMTFP2TracksEnabled")) {
        data_collectors_.push_back(std::make_unique<EMTFP2TracksCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("EMTFP2SimInfoEnabled")) {
        data_collectors_.push_back(std::make_unique<EMTFP2SimInfoCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("L1TrackTriggerTracksEnabled")) {
        data_collectors_.push_back(std::make_unique<L1TrackTriggerCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("TrackingParticlesEnabled")) {
        data_collectors_.push_back(std::make_unique<TrackingParticlesCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("CSCSimHitEnabled")) {
        data_collectors_.push_back(std::make_unique<CSCSimHitCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("RPCSimHitEnabled")) {
        data_collectors_.push_back(std::make_unique<RPCSimHitCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("GEMSimHitEnabled")) {
        data_collectors_.push_back(std::make_unique<GEMSimHitCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("ME0SimHitEnabled")) {
        data_collectors_.push_back(std::make_unique<ME0SimHitCollector>(
            context_, pset, consumesCollector()
        ));
    }

    if (pset.getParameter<bool>("GenParticlesEnabled")) {
        data_collectors_.push_back(std::make_unique<GenParticlesCollector>(
            context_, pset, consumesCollector()
        ));
    }
}

EMTFToolsNtupleMaker::~EMTFToolsNtupleMaker() {
    // Do nothing
}

void EMTFToolsNtupleMaker::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
    edm::ParameterSetDescription desc;
    desc.setUnknown();
    descriptions.addDefault(desc);
}

void EMTFToolsNtupleMaker::beginJob() {
    // Register fields
    for (auto& data_collector : data_collectors_) {
        data_collector->registerFields(tree_);
    }
}

void EMTFToolsNtupleMaker::beginRun(const edm::Run& run, const edm::EventSetup& event_setup) {
    // Do nothing
}

void EMTFToolsNtupleMaker::analyze(const edm::Event& event, const edm::EventSetup& event_setup) {
    // Update Context
    context_.update(event, event_setup);

    // Clear tree
    tree_.clear();

    // Run all data collectors
    for (auto& data_collector : data_collectors_) {
        data_collector->collect(event, event_setup, tree_);
    }

    // Fill tree
    tree_.fill();
}

void EMTFToolsNtupleMaker::endRun(const edm::Run& run, const edm::EventSetup& event_setup) {
    // Do nothing
}

void EMTFToolsNtupleMaker::endJob() {
    tree_.write();
}

//define this as a plug-in
DEFINE_FWK_MODULE(EMTFToolsNtupleMaker);
